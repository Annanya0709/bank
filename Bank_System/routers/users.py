from fastapi import APIRouter, Depends, HTTPException, status
from schemas import UserCreate, ShowUser
from sqlalchemy.orm import Session
from typing import List
from database import get_db
from hashing import Hasher
from models import User
import random

router = APIRouter()

@router.get('/users')
def get_user():
    return {"Message": "Hello user"}

#  token: str = Depends(oath2_scheme)
@router.post('/register', response_model= ShowUser)
def create_user( db: Session = Depends(get_db)):
    account_no = random.randint(100000000000,999999999999)
    user = User(username = user.username, password = Hasher.get_hash_password(user.password), account_no = account_no)
    db.add(user)
    db.commit()
    db.refresh(user)
    return user

# token: str = Depends(oauth2_scheme)
@router.get('/users/all', response_model= List[ShowUser])
def get_all_users(db:Session=Depends(get_db)):
    user = db.query(User).all()
    return user

    # try:
    #     payload = jwt.decode(token, settings.SECRET_KEY, algorithms=settings.ALGORITHM)
    #     username = payload.get('sub')
    #     print(username)
    #     if username is None:
    #         raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail = 'Unable to verify credentials not a username')
    #     user = db.query(User).filter(User.username == username).first()
       
    #     if user is None:
    #         raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail = 'Unable to verify credentials user is none')

    # except Exception as e:
    #         raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail = 'Unable to verify credentials')


# token: str = Depends(oauth2_scheme)
@router.get('/users/{id}', response_model=ShowUser)
def get_user_by_id(id:int, db:Session=Depends(get_db)):
    # try:
    #     payload = jwt.decode(token, settings.SECRET_KEY, algorithms=settings.ALGORITHM)
    #     username = payload.get('sub')
    #     print(username)
    #     if username is None:
    #         raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail = 'Unable to verify credentials not a username')
    #     user = db.query(User).filter(User.username == username).first()
    #     if user is None:
    #         raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail = 'Unable to verify credentials user is none')

    # except Exception as e:
    #     raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail = 'Unable to verify credentials')

    user = db.query(User).filter(User.id == id).first()
    if not user:
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND, details = f"item {id} does not exists")
    return user

