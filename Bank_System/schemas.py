from pydantic import BaseModel, EmailStr

class UserCreate(BaseModel):
    username: EmailStr
    password: str
    account_no = int

class ShowUser(BaseModel):
    username: EmailStr
    account_no: int

    class Config:
        orm_mode = True

class Account(BaseModel):
    account_no: int
    curr_balance: float

class ShowAccount(BaseModel):
    account_no: int
    curr_balance: float

    class Config:
        orm_mode = True


