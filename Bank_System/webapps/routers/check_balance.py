from fastapi import APIRouter, Depends, Request, Form, status

from sqlalchemy.orm import Session
from schemas import ShowAccount
from database import get_db
from models import Account
from hashing import Hasher
from starlette.templating import Jinja2Templates

templates = Jinja2Templates(directory="templates")

router = APIRouter()

@router.get("/balance")
def balance(request: Request):
    return templates.TemplateResponse("home.html", {"request": request})

# @router.post("/balance/{id}")
# async def check_balance( db:Session = Depends(), response_model = ShowAccount):

#     accounts = db.query(Account).filter(Account.user_id == id).first()
#     return accounts
